package az.ingress.domain;

public enum Role {
    USER,
    ADMIN
}