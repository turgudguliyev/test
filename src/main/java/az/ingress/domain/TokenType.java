package az.ingress.domain;

public enum TokenType {
    ACCESS_TOKEN,
    REFRESH_TOKEN
}