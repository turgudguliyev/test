package az.ingress.controller;

import az.ingress.dto.request.RefreshTokenRequest;
import az.ingress.dto.request.SignInRequest;
import az.ingress.dto.request.SignUpRequest;
import az.ingress.dto.response.SignInResponse;
import az.ingress.dto.response.SignUpResponse;
import az.ingress.service.auth.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService service;

    @PostMapping("/register")
    public SignUpResponse register(
            @RequestBody SignUpRequest request
    ) {
        return service.register(request);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<SignInResponse> authenticate(
            @RequestBody SignInRequest request
    ) {
        return ResponseEntity.ok(service.authenticate(request));
    }

    @PostMapping("/refresh")
    public SignInResponse refresh(@RequestBody RefreshTokenRequest dto) {
        return service.refreshToken(dto);
    }
}