package az.ingress.service.auth;

import az.ingress.domain.*;
import az.ingress.dto.request.RefreshTokenRequest;
import az.ingress.dto.request.SignInRequest;
import az.ingress.dto.request.SignUpRequest;
import az.ingress.dto.response.SignInResponse;
import az.ingress.dto.response.SignUpResponse;
import az.ingress.exception.NotFoundException;
import az.ingress.exception.ValidationException;
import az.ingress.repository.TokenRepository;
import az.ingress.repository.UserRepository;
import az.ingress.service.jwt.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

import static az.ingress.domain.TokenType.ACCESS_TOKEN;
import static az.ingress.domain.TokenType.REFRESH_TOKEN;
import static lombok.AccessLevel.PRIVATE;

@Service
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = PRIVATE)
public class AuthenticationService {
    JwtService jwtService;
    UserRepository repository;
    TokenRepository tokenRepository;
    PasswordEncoder passwordEncoder;
    AuthenticationManager authenticationManager;

    public SignUpResponse register(SignUpRequest request) {
        Authority authority = Authority.builder()
                .authority(Role.USER)
                .build();
        var user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .authorities(Set.of(authority))
                .birthDate(request.getBirthDate())
                .build();
        authority.setUser(user);
        repository.save(user);
        return SignUpResponse.builder()
                .message("Your registration has been successfully completed.")
                .build();
    }

    public SignInResponse authenticate(SignInRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );
        User user = checkIfExists(request.getUsername());
        return getSignInResponse(user);
    }


    public SignInResponse refreshToken(RefreshTokenRequest dto) {
        String username = jwtService.extractUsername(dto.getToken());
        var user = checkIfExists(username);
        if (!jwtService.isTokenValid(dto.getToken(), user)) {
            throw new ValidationException("TOKEN_NOT_VALID");
        }
        return getSignInResponse(user);
    }

    private SignInResponse getSignInResponse(User user) {
        var accessToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        saveTokens(user, accessToken, refreshToken);
        return SignInResponse.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
    }

    private void saveTokens(User user, String accessToken, String refreshToken) {
        saveUserToken(user, accessToken, ACCESS_TOKEN);
        saveUserToken(user, refreshToken, REFRESH_TOKEN);
    }

    private void saveUserToken(User user, String jwtToken, TokenType type) {
        var token = Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(type)
                .build();
        tokenRepository.save(token);
    }

    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

    private User checkIfExists(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException("USER_NOT_FOUND"));
    }
}
