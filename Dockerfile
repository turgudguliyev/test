FROM gradle as builder
ADD --chown=gradle . /app
WORKDIR /app
RUN gradle build -x test
FROM openjdk
WORKDIR /app
COPY --from=builder /app/build/libs/*.jar .
ENV JVM_OPTS="-Dspring.profiles.active=dev"
EXPOSE 8888
ENTRYPOINT exec java $JVM_OPTS -ea -jar /app/*.jar